import React from "react";

const GraphTemplatesSelector = ({
  templates,
  enabledTemplateNames,
  setEnabledTemplateNames,
}) => {
  const handleChangeActiveTemplates = (e) => {
    const clickedTemplateName = e.target.name;
    const updatedTemplateNames = e.target.checked
      ? [...enabledTemplateNames, clickedTemplateName]
      : enabledTemplateNames.filter((name) => name !== clickedTemplateName);

    setEnabledTemplateNames(updatedTemplateNames);
  };

  return (
    <>
      <span className="font-bold">Enabled graphs:</span>
      <form className="flex flex-col">
        {templates.map((template) => (
          <div key={template.name}>
            <label className="inline-flex items-center">
              <input
                type="checkbox"
                className="form-checkbox"
                name={template.name}
                checked={enabledTemplateNames.includes(template.name)}
                onChange={handleChangeActiveTemplates}
              />
              <span className="ml-2 capitalize">{template.name}</span>
            </label>
          </div>
        ))}
      </form>
    </>
  );
};

export default GraphTemplatesSelector;
