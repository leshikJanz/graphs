import React, { useState } from "react";
import { getIsXAxisFormValid } from "../utils";

const XAxisParamsForm = ({ xAxisParams, setXAxisParams }) => {
  const [startXPoint, setStartXPoint] = useState(xAxisParams.startXPoint);
  const [endXPoint, setEndXPoint] = useState(xAxisParams.endXPoint);
  const [stepByX, setStepByX] = useState(xAxisParams.stepByX);

  const handleSubmit = (e) => {
    e.preventDefault();
    setXAxisParams({
      startXPoint: Number(startXPoint),
      endXPoint: Number(endXPoint),
      stepByX: Number(stepByX),
    });
  };

  const isFormValid = getIsXAxisFormValid({
    startXPoint: Number(startXPoint),
    endXPoint: Number(endXPoint),
    stepByX: Number(stepByX),
  });

  return (
    <form
      className="xAxisForm flex md:flex-row flex-col"
      noValidate
      onSubmit={handleSubmit}
    >
      <div className="mr-4 mt-4">
        <label className="block text-gray-700 text-sm font-bold mb-2">
          Starting X point
        </label>
        <input
          className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
          value={startXPoint || ""}
          onChange={(e) => setStartXPoint(e.target.value)}
          type="number"
          autoFocus
          placeholder="0"
        />
      </div>
      <div className="mr-4 mt-4">
        <label className="block text-gray-700 text-sm font-bold mb-2">
          Ending X point
        </label>
        <input
          className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
          value={endXPoint || ""}
          onChange={(e) => setEndXPoint(e.target.value)}
          type="number"
          placeholder="0"
        />
      </div>
      <div className="mr-4 mt-4">
        <label className="block text-gray-700 text-sm font-bold mb-2">
          Step
        </label>
        <input
          className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
          type="number"
          value={stepByX || ""}
          onChange={(e) => setStepByX(e.target.value)}
          placeholder="0"
        />
      </div>
      <button
        type="submit"
        className="h-10 mt-4 self-start md:self-end bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline disabled:bg-gray-50"
        disabled={!isFormValid}
      >
        Submit
      </button>
    </form>
  );
};

export default XAxisParamsForm;
