import { throttle } from "lodash";
import React, { useEffect, useState } from "react";
import {
  XYPlot,
  XAxis,
  YAxis,
  VerticalGridLines,
  HorizontalGridLines,
  LineSeries,
} from "react-vis";

import {
  defaultPlotWidth,
  defaultPlotHeight,
  throttleWaitTime,
  plotRightPadding,
  defaultYDomain,
} from "../constants";
import { getLineSeriesData } from "../utils";

const Plot = ({
  xDomain,
  yDomain = defaultYDomain,
  xTickValues,
  templates,
}) => {
  const [plotWidth, setPlotWidth] = useState(defaultPlotWidth);

  useEffect(() => {
    handleResize();
    window.addEventListener("resize", handleResize);
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  const handleResize = throttle(
    () => setPlotWidth(window.innerWidth - plotRightPadding),
    throttleWaitTime
  );

  return (
    <XYPlot
      width={plotWidth}
      height={defaultPlotHeight}
      {...{ xDomain, yDomain }}
    >
      <VerticalGridLines />
      <HorizontalGridLines />
      <XAxis on0 tickValues={xTickValues} />
      <YAxis on0 />
      {templates.map((template) => (
        <LineSeries
          key={template.name}
          data-test-id={`line-series-${template.name}`}
          color={template.color}
          data={getLineSeriesData(template)}
        />
      ))}
    </XYPlot>
  );
};

export default Plot;
