import React, { useState } from "react";

import TemplateSelector from "./TemplateSelector";
import Plot from "./Plot";
import XAxisParamsForm from "./XAxisParamsForm";

import "../style.css";
import { getXTickValues } from "../utils";
import { mockGraphTemplates } from "../mocks";

const Graph = () => {
  const [xAxisParams, setXAxisParams] = useState({});
  const [enabledTemplateNames, setEnabledTemplateNames] = useState(
    mockGraphTemplates.map((template) => template.name)
  );

  const enabledTemplates = mockGraphTemplates.filter((template) =>
    enabledTemplateNames.includes(template.name)
  );
  const { startXPoint, endXPoint } = xAxisParams;

  return (
    <>
      <XAxisParamsForm
        xAxisParams={xAxisParams}
        setXAxisParams={setXAxisParams}
      />
      {startXPoint !== undefined && endXPoint !== undefined && (
        <div className="mt-8">
          <TemplateSelector
            templates={mockGraphTemplates}
            enabledTemplateNames={enabledTemplateNames}
            setEnabledTemplateNames={setEnabledTemplateNames}
          />
          <div className="mt-8">
            <Plot
              xDomain={[startXPoint, endXPoint]}
              xTickValues={getXTickValues(xAxisParams)}
              templates={enabledTemplates}
            />
          </div>
        </div>
      )}
    </>
  );
};

export default Graph;
