export const defaultPlotWidth = 300;
export const defaultPlotHeight = 300;

export const minXStepValue = 0.1;

export const throttleWaitTime = 300;

export const plotRightPadding = 80;
export const defaultYDomain = [-10, 10];

export const dividerForRounding = 10;
