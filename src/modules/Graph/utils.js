import { dividerForRounding, minXStepValue } from "./constants";

export const getIsXAxisFormValid = ({ startXPoint, endXPoint, stepByX }) => {
  return (
    startXPoint !== undefined &&
    endXPoint !== undefined &&
    startXPoint !== "" &&
    endXPoint !== "" &&
    stepByX !== "" &&
    startXPoint < endXPoint &&
    startXPoint !== endXPoint &&
    stepByX >= minXStepValue &&
    stepByX < endXPoint - startXPoint
  );
};

export const getXTickValues = ({ startXPoint, endXPoint, stepByX }) => {
  if (!startXPoint || !endXPoint || !stepByX) return [];

  const ticksCount = Math.floor((endXPoint - startXPoint + stepByX) / stepByX);

  return Array(ticksCount)
    .fill(startXPoint)
    .map(
      (point, index) =>
        Math.floor((point + index * stepByX) * dividerForRounding) /
        dividerForRounding
    );
};

export const getLineSeriesData = (template) =>
  template.x.map((x, index) => ({ x, y: template.y[index] }));
