import React from "react";
import { render } from "@testing-library/react";

import Plot from "../components/Plot";
import { mockGraphTemplates } from "../mocks";

describe("Plot", () => {
  const xDomain = [-5, 5];
  const xTickValues = [-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5];

  it("matches Plot snapshot with 5 mocked templates", () => {
    const { container } = render(
      <Plot
        xDomain={xDomain}
        xTickValues={xTickValues}
        templates={mockGraphTemplates}
      />
    );

    expect(container).toMatchSnapshot();
  });

  it("should have 5 graphs as templates count", () => {
    const { container } = render(
      <Plot
        xDomain={xDomain}
        xTickValues={xTickValues}
        templates={mockGraphTemplates}
      />
    );

    const lineSeries = container.querySelectorAll(
      "path.rv-xy-plot__series.rv-xy-plot__series--line"
    );

    expect(lineSeries.length).toBe(mockGraphTemplates.length);
  });
});
