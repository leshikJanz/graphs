import {
  getIsXAxisFormValid,
  getLineSeriesData,
  getXTickValues,
} from "../utils";

describe("getIsXAxisFormValid", () => {
  const startXPoint = -5;
  const endXPoint = 5;
  const stepByX = 1;

  it("should return false if startXPoint is undefined", () => {
    expect(
      getIsXAxisFormValid({ startXPoint: undefined, endXPoint, stepByX })
    ).toBeFalsy();
  });

  it("should return false if endXPoint is undefined", () => {
    expect(
      getIsXAxisFormValid({ startXPoint, endXPoint: undefined, stepByX })
    ).toBeFalsy();
  });

  it("should return false if startXPoint is empty string '' ", () => {
    expect(
      getIsXAxisFormValid({ startXPoint: "", endXPoint, stepByX })
    ).toBeFalsy();
  });

  it("should return false if endXPoint is empty string '' ", () => {
    expect(
      getIsXAxisFormValid({ startXPoint, endXPoint: "", stepByX })
    ).toBeFalsy();
  });

  it("should return false if stepByX is empty string '' ", () => {
    expect(
      getIsXAxisFormValid({ startXPoint, endXPoint, stepByX: "" })
    ).toBeFalsy();
  });

  it("should return false if startXPoint is more than endXPoint", () => {
    expect(
      getIsXAxisFormValid({ startXPoint: 10, endXPoint: 5, stepByX })
    ).toBeFalsy();
  });

  it("should return false if startXPoint equals endXPoint", () => {
    expect(
      getIsXAxisFormValid({ startXPoint: 10, endXPoint: 10, stepByX })
    ).toBeFalsy();
  });

  it("should return false if stepByX less than minXStepValue", () => {
    expect(
      getIsXAxisFormValid({ startXPoint, endXPoint, stepByX: 0 })
    ).toBeFalsy();
  });

  it("should return false if stepByX more than endXPoint - startXPoint", () => {
    expect(
      getIsXAxisFormValid({ startXPoint: 5, endXPoint: 10, stepByX: 10 })
    ).toBeFalsy();
  });

  it("should return true if attributes are valid", () => {
    expect(
      getIsXAxisFormValid({ startXPoint, endXPoint, stepByX })
    ).toBeTruthy();
  });
});

describe("getXTickValues", () => {
  const startXPoint = -5;
  const endXPoint = 5;
  const stepByX = 1;

  it("should return empty array [] if !startXPoint", () => {
    expect(
      getXTickValues({ startXPoint: undefined, endXPoint, stepByX })
    ).toEqual([]);
  });

  it("should return empty array [] if !endXPoint", () => {
    expect(
      getXTickValues({ startXPoint, endXPoint: undefined, stepByX })
    ).toEqual([]);
  });

  it("should return empty array [] if !stepByX", () => {
    expect(
      getXTickValues({ startXPoint, endXPoint, stepByX: undefined })
    ).toEqual([]);
  });

  it("should return tickValues array from -5 to 5 counted by 1 if attributes are correct", () => {
    expect(getXTickValues({ startXPoint, endXPoint, stepByX })).toEqual([
      -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5,
    ]);
  });
});

describe("getLineSeriesData", () => {
  const testTemplate = {
    x: [0, 1, 2, 3],
    y: [3, 2, 1, 0],
  };
  it("should convert template into array like [{ x, y }]", () => {
    expect(getLineSeriesData(testTemplate)).toEqual([
      { x: 0, y: 3 },
      { x: 1, y: 2 },
      { x: 2, y: 1 },
      { x: 3, y: 0 },
    ]);
  });
});
