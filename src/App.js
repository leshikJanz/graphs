import React from "react";

import Graph from "./modules/Graph";

const App = () => (
  <div className="m-4 md:m-8">
    <Graph />
  </div>
);

export default App;
