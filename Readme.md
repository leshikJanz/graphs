**Run locally: **  
1. yarn && yarn start  
2. open [localhost:8080](http://localhost:8080 "localhost:8080")  

------------

**Also deployed version could be found here:**  
[https://react-janz-boilerplate.herokuapp.com/](https://react-janz-boilerplate.herokuapp.com/ "https://react-janz-boilerplate.herokuapp.com/")

------------

**Run tests: **  yarn test 